﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie1
{
    class Program
    {
        static void Main(string[] args)
        {
            var w1 = new Wektor(1, 2, 3, 4);
            var w1a = new Wektor(1, 2, 3, 4);
            var w2 = new Wektor(2, 4, 6, 8);
            var w3 = new Wektor(1, 1, 1, 1);

            List<Wektor> listaWektorow = new List<Wektor>();
            listaWektorow.Add(w1);
            listaWektorow.Add(w2);
            listaWektorow.Add(w3);

            listaWektorow.Sort();
            Console.WriteLine(w1 == w1a);
            Console.WriteLine(w1 != w1a);
            Console.WriteLine(w1 != w1a);
            Console.WriteLine(w1-w2);
            Console.WriteLine(w2 + w1);
            Console.WriteLine(w1.Proporcjonalny(w2));
            Console.WriteLine(w1.Proporcjonalny(w2));
            Console.WriteLine(w2.Proporcjonalny(w3));
            Console.WriteLine(w2.Proporcjonalny(w1));
            Console.ReadKey();
        }
    }
}
