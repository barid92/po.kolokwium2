﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie1
{
    class Wektor : IProporcjonalny,IComparable
    {
        int[] myTab;
        public Wektor(int x, int y, int z, int t)
        {
            myTab = new int[4];
            myTab[0] = x;
            myTab[1] = y;
            myTab[2] = z;
            myTab[3] = t;
        }

        public int CompareTo(object obj)
        {

            int w1 = myTab[0] + myTab[1] + myTab[2] + myTab[3];
            var wektorObj = (Wektor)obj;
            int w2 = wektorObj.myTab[0] + wektorObj.myTab[1] + wektorObj.myTab[2] + wektorObj.myTab[3];

            return w1 - w2;
        }

        public  bool Proporcjonalny(Wektor w2)
        {
            var w1 = this;
            double wspolczynnikX = w1.myTab[0] / w2.myTab[0];
            double wspolczynnikY = w1.myTab[1] / w2.myTab[1];
            double wspolczynnikZ = w1.myTab[2] / w2.myTab[2];
            double wspolczynnikT = w1.myTab[3] / w2.myTab[3];
            if (wspolczynnikX == wspolczynnikY && wspolczynnikX == wspolczynnikZ && wspolczynnikX == wspolczynnikT)
            {
                return true;
            }
            return false;
        }

        public static Wektor operator +(Wektor w1,Wektor w2)
        {
            return new Wektor(w1.myTab[0] + w2.myTab[0], w1.myTab[1] + w2.myTab[1], w1.myTab[2] + w2.myTab[2], w1.myTab[3] + w2.myTab[3]);
        }
        public static Wektor operator -(Wektor w1, Wektor w2)
        {
            return new Wektor(w1.myTab[0] - w2.myTab[0], w1.myTab[1] - w2.myTab[1], w1.myTab[2] - w2.myTab[2], w1.myTab[3] - w2.myTab[3]);
        }
        public static Wektor operator *(Wektor w1, int liczba)
        {
            return new Wektor(w1.myTab[0] * liczba, w1.myTab[1] * liczba, w1.myTab[2] * liczba, w1.myTab[3] * liczba);
        }
        public static Wektor operator /(Wektor w1, int liczba)
        {
            if (liczba == 0)
            {
                throw new Exception("Pamietaj cholero nie dziel przez zero");
            }
            return new Wektor(w1.myTab[0] / liczba, w1.myTab[1] / liczba, w1.myTab[2] / liczba, w1.myTab[3] / liczba);
        }
        public static Wektor operator %(Wektor w1, int liczba)
        {
            if (liczba == 0)
            {
                throw new Exception("Pamietaj cholero nie dziel przez zero");
            }
            return new Wektor(w1.myTab[0] % liczba, w1.myTab[1] % liczba, w1.myTab[2] % liczba, w1.myTab[3] % liczba);
        }

        public static bool operator ==(Wektor w1, Wektor w2)
        {
            if (w1.myTab[0] == w2.myTab[0] && w1.myTab[1] == w2.myTab[1]&& w1.myTab[2] == w2.myTab[2]&& w1.myTab[3] == w2.myTab[3])
            {
                return true;
            }
            return false;
        }
        public static bool operator !=(Wektor w1, Wektor w2)
        {

            if (w1.myTab[0] == w2.myTab[0] && w1.myTab[1] == w2.myTab[1] && w1.myTab[2] == w2.myTab[2] && w1.myTab[3] == w2.myTab[3])
            {
                return false;
            }
            return true;
        }
        public override string ToString()
        {
            return String.Format("[{0};{1};{2};{3}]",myTab[0], myTab[1],myTab[2],myTab[3]);
        }
    }
}
