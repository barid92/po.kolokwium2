﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie2
{
    [Serializable]
    class Student : Osoba
    {
        public string NumerAlbumu;
        public Student(string na, string i, string n, string p) : base(i, n, p)
        {
            NumerAlbumu = na;
        }
    }
}
