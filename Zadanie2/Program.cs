﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie2
{
    class Program
    {
        static void Serializable(string nazwaPliku,object obj)
        {
            var fs = new FileStream(nazwaPliku, FileMode.Create);
            var bf = new BinaryFormatter();
            bf.Serialize(fs, obj);
            fs.Close();
        }
        static T Deserializable<T>(string nazwaPliku)
        {
            var fs = new FileStream(nazwaPliku, FileMode.Open);
            var bf = new BinaryFormatter();
            return (T)bf.Deserialize(fs);
            
            
        }
        static void Main(string[] args)
        {
            var stos = new Stos<string>();
            stos.Poloz("Test");
            //Console.WriteLine(stos.Podejrzyj());
            stos.Poloz("Test1");
            stos.Poloz("Test2");
            foreach (var item in stos)
            {
                Console.WriteLine(item);
            }
            //Console.WriteLine(stos.Podejrzyj());
            //Console.WriteLine(stos.Zdejmij());
            //.WriteLine(stos.Rozmiar());


            var s1 = new Student("666", "Piotr", "AJ", "12345678909");
            var s2 = new Student("jfdiok", "Piotr", "WD40", "17845678909");

            var StosStudentow = new Stos<Student>();
            StosStudentow.Poloz(s1);
            StosStudentow.Poloz(s2);

            Serializable("test.bin", StosStudentow);

            var nowyStos = Deserializable<Stos<Student>>("test.bin");
            Console.ReadKey();
        }
    }
}
