﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie2
{
    [Serializable]
    class Stos<T> : IStos<T>, IEnumerable<T>
    {
        LinkedList<T> myList;
        int myIndex;
        public Stos()
        {
            myIndex = 0;
            myList = new LinkedList<T>();
        }

        public T Podejrzyj()
        {
            if (myIndex == 0)
            {
                throw new Exception();
            }
            return myList.ElementAt(myIndex-1);
        }

        public T Poloz(T i)
        {
            myList.AddLast(i);
            myIndex++;
            return i;

            
        }

        public int Rozmiar()
        {
            return myList.Count();
        }

        public T Zdejmij()
        {
            var temp = myList.Last();
            myList.RemoveLast();
            return temp;
        }

        public IEnumerator<T> GetEnumerator()
        {
            //Mozna to na dwa sposoby zrobic ale uzylem prostrzego <CIEKAWE>
            //foreach (var item in myList)
            //{
            //    yield return item;
            //}
            return myList.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
