﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie2
{
    interface IStos<T>
    {
        int Rozmiar(); //podaje rozmiar
        T Poloz(T i); //kładzie element na stos zwraca położony element
        T Zdejmij(); // zdejmuje element z wierzchu stosu i zwraca zdjęty
        T Podejrzyj(); //odczytuje element z wierzchu stosu
    }
}
