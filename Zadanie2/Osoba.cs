﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie2
{
    [Serializable]
    class Osoba
    {
        public string Imie;
        public string Nazwisko;
        public string Pesel;

        public Osoba (string i,string n , string p )
        {
            Imie = i;
            Nazwisko = n;
            Pesel = p;
        }
    }
}
