﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie3
{
    class Program
    {
        static void Main(string[] args)
        {
            var nowySklep = new Sklep();
            nowySklep.WejscieKlienta();
            nowySklep.WejscieKlienta();
            nowySklep.WyjscieKlienta();
            Console.ReadKey();
        }
    }
    class Sklep
    {
        public Sklep()
        {
            Zmiana += this.Alarm;
        }
        int liczbaKientow;
        public event EventHandler Zmiana;
        public void Alarm(object sender, EventArgs args)
        {
            Console.WriteLine("Ding Dong");
            Console.WriteLine("Liczba klientow to " + liczbaKientow);
        }
        public void WejscieKlienta()
        {
            this.liczbaKientow++;
            Alarm(this, new EventArgs());


       
        }
        public void WyjscieKlienta()
        {
            if (liczbaKientow == 0)
            {
                throw new Exception();
            }
            this.liczbaKientow--;
            Alarm(this, new EventArgs());
        }
    }
}
