﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie4
{
    class Program
    {
        static void Main(string[] args)
        {
            string dllPath = Directory.GetCurrentDirectory() + "\\" + "Zadanie4DLL.dll";
            CzyMetoda(dllPath, "TegoSzukasza");

            Console.ReadKey();
        }
        static void CzyMetoda(string path,string method)
        {
            Assembly assembly = Assembly.LoadFile(path);
            var myMethod= assembly.GetTypes()[0].GetMethod(method);
            if (myMethod == null)
            {
                Console.WriteLine("Brak metody o podanej nazwie");
                return;
            }
            myMethod.Invoke(Activator.CreateInstance(assembly.GetTypes()[0]),null);
        }
    }
}
